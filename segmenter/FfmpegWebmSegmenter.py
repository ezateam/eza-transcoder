__author__ = 'Zangue'

import subprocess, shlex

from BaseSegmenter import BaseSegmenter

class FfmpegWebmSegmenter(BaseSegmenter):

    def __init__(self):
        self._config = None
        self._command = 'ffmpeg'
        self._container = 'webm'

    def set_config(self, config):
        self._config = config

    def support_container(self, container):
        return container is self._container

    def output_dash(self):
        cmd = []
        video_tracks = self._config['video_tracks']
        audio_tracks = self._config['audio_tracks']
        nr_video_tracks = len(video_tracks)
        nr_audio_tracks = len(audio_tracks)
        nr_tracks = nr_video_tracks + nr_audio_tracks
        as_id = 0
        stream_id = 0

        cmd.append(self._command)

        for video_track in video_tracks:
            cmd.append('-f')
            cmd.append('webm_dash_manifest')
            cmd.append('-i')
            cmd.append(video_track)

        for audio_track in audio_tracks:
            cmd.append('-f')
            cmd.append('webm_dash_manifest')
            cmd.append('-i')
            cmd.append(audio_track)

        cmd.append('-c')
        cmd.append('copy')

        for x in range(0, nr_tracks):
            cmd.append('-map')
            cmd.append(str(x))

        cmd.append('-f')
        cmd.append('webm_dash_manifest')
        cmd.append('-adaptation_sets')

        # Video adaptation set
        as_config = '"id=' + str(as_id) + ',streams='
        as_id += 1

        for x in range(0, nr_video_tracks):
            as_config += str(stream_id)
            stream_id += 1
            if x < nr_video_tracks - 1:
                as_config += ','

        # Audio adaptation set
        as_config += ' id=' + str(as_id) + ',streams='
        as_id += 1

        for x in range(0, nr_audio_tracks):
            as_config += str(stream_id)
            stream_id += 1
            if x < nr_audio_tracks - 1:
                as_config += ','

        as_config += '"'

        cmd.append(as_config)
        cmd.append(self._config['output_file']) # Manifest

        cmd_string = ' '.join(str(x) for x in cmd)

        print "[FfmpegWebmSegmenter] segment files"
        print "[FfmpegWebmSegmenter] Command: %s" % cmd_string

        sp = subprocess.Popen(shlex.split(cmd_string))
        sp.wait()

        return sp.returncode