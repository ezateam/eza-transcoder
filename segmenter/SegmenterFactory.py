__author__ = 'Zangue'

from FfmpegWebmSegmenter import FfmpegWebmSegmenter

class SegmenterFactory(object):

    @staticmethod
    def get_segmenter(container):
        segmenters = {
            'webm': FfmpegWebmSegmenter
        }

        segmenter = segmenters.get(container, None)

        if segmenter is None:
            raise ValueError('Could not find segmenter for container %s' % container)

        return segmenter()