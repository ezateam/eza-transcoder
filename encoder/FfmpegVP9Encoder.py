__author__ = 'Zangue'

import subprocess

from BaseEncoder import BaseEncoder

class FfmpegVP9Encoder(BaseEncoder):

    def __init__(self):
        self._config = None
        self._supported_codec = 'vp9' 
        #self._vp9_dash_params = '-tile-columns 4 -frame-parallel 1'
        self._vp9_dash_params = ['-tile-columns', '4', '-frame-parallel', '1']
        self._command = 'ffmpeg'

    def set_config(self, config):
        self._config = config

    def support_codec(self, codec):
        return codec is self._supported_codec

    def encode(self):
        cmd = []

        cmd.append(self._command)
        cmd.append('-i')
        cmd.append(self._config['input_file'])
        cmd.append('-map')
        cmd.append('0:0')
        cmd.append('-c:v')
        cmd.append('libvpx-vp9')
        cmd.append('-s')
        cmd.append(str(self._config['height']) + 'x' + str(self._config['width']))
        cmd.append('-b:v')
        cmd.append(str(self._config['bitrate']) + 'k')
        cmd.append('-keyint_min')
        cmd.append(str(self._config['keyint_min']))
        cmd.append('-g')
        cmd.append(str(self._config['keyint_min']))
        #cmd.append(self._vp9_dash_params)
        cmd += self._vp9_dash_params
        cmd += ['-an', '-f', 'webm', '-dash', '1']
        cmd.append(self._config['output_file'])

        print "[FfmpegVP9Encoder] encoding video @ %d kbps" % self._config['bitrate']
        print "[FfmpegVP9Encoder] Command: %s" % ' '.join(str(x) for x in cmd)

        sp = subprocess.Popen(cmd)
        sp.wait()

        return sp.returncode