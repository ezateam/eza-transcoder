__author__ = 'Zangue'

from FfmpegVP9Encoder import FfmpegVP9Encoder
from FfmpegVorbisEncoder import FfmpegVorbisEncoder

class EncoderFactory(object):

    @staticmethod
    def get_encoder(codec):
        encoders = {
            'vp9': FfmpegVP9Encoder,
            'vorbis': FfmpegVorbisEncoder
        }

        encoder = encoders.get(codec, None)

        if encoder is None:
            raise ValueError('Could not find encoder for codec %s' % codec)

        return encoder()
            

