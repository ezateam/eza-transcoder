__author__ = 'Zangue'

import subprocess

from BaseEncoder import BaseEncoder

class FfmpegVorbisEncoder(BaseEncoder):

    def __init__(self):
        self._config = None
        self._supported_codec = 'vorbis'
        self._command = 'ffmpeg'

    def set_config(self, config):
        self._config = config

    def support_codec(self, codec):
        return codec is self._supported_codec

    def encode(self):
        cmd = []

        cmd.append(self._command)
        cmd.append('-i')
        cmd.append(self._config['input_file'])
        cmd.append('-c:a')
        cmd.append('libvorbis')
        cmd.append('-b:a')
        cmd.append(str(self._config['bitrate']) + 'k')
        #cmd.append('-vn -f webm -dash 1')
        cmd += ['-vn', '-f', 'webm', '-dash', '1']
        cmd.append(self._config['output_file'])

        print "[FfmpegVP9Encoder] encoding audio @ %d kbps" % self._config['bitrate']
        print "[FfmpegVP9Encoder] Command: %s" % ' '.join(str(x) for x in cmd)

        sp = subprocess.Popen(cmd)
        sp.wait()

        return sp.returncode