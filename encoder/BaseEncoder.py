__author__ = 'Zangue'

from abc import ABCMeta, abstractmethod

class BaseEncoder(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def set_config(self, config):
        pass

    @abstractmethod
    def support_codec(self, codec):
        pass

    @abstractmethod
    def encode(self):
        pass