__author__ = 'Zangue'

class SegmenterConfig(object):

    def __init__(self, video_tracks, audio_tracks, output_file='Manifest.mpd'):
        self.video_tracks = video_tracks
        self.audio_tracks = audio_tracks
        self.output_file = output_file

    def toDict(self):
        return self.__dict__