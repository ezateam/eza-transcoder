__author__ = 'Zangue'

class AudioStreamConfig(object):

    def __init__(self, input_file, bitrate, output_file):
        self.input_file = input_file
        self.bitrate = bitrate
        self.output_file = output_file

    def toDict(self):
        return self.__dict__