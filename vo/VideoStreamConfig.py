__author__ = 'Zangue'

class VideoStreamConfig(object):

    def __init__(self, input_file, bitrate, keyint_min, height, width, output_file):
        self.input_file = input_file
        self.bitrate = bitrate
        self.keyint_min = keyint_min
        self.height = height
        self.width = width
        self.output_file = output_file

    def toDict(self):
        return self.__dict__