
"""
Example of program with many options using docopt.

Usage:
    etranscoder.py [options] [-q | -v]
    etranscoder.py --version

Options:
    -h --help                   show this help message and exit
    --version                   show version and exit
    -v --verbose                print status messages
    -q --quiet                  report only file names

    -i INPUT                    input file
    -b VALUE                    source video bitrate
    --keyint-min=KIM            minimal interval between key frames
    --movie-name=NAME           output dir name
    --audio-file=AFILE          audio file
    --audio-bitrate=AVALUE      audio source bitrate
"""

__author__ = 'Zangue'

from docopt import docopt
from schema import Schema, And, Or, Use, SchemaError

import os
import sys

from encoder.EncoderFactory import EncoderFactory
from segmenter.SegmenterFactory import SegmenterFactory
from analyzer.SourceAnalyzer import SourceAnalyzer
from vo.AudioStreamConfig import AudioStreamConfig
from vo.VideoStreamConfig import VideoStreamConfig
from vo.SegmenterConfig import SegmenterConfig

__version__ = '0.1'

TMPDIR = '/tmp'

def make_dir(path):
    os.makedirs(path)

if __name__ == '__main__':
    args = docopt(__doc__, version=__version__)
    print(args)
    #sys.exit(1)
    vbitrates = [250, 500]
    abitrates = [128]

    resolutions = {'250':[160, 90], '500':[320, 180]}

    video_configs = []
    audio_configs = []

    outdir = TMPDIR + '/' + args['--movie-name']
    outdir_video = outdir + '/video'
    outdir_audio = outdir + '/audio'

    make_dir(outdir)
    make_dir(outdir_video)
    make_dir(outdir_audio)

    for vbitrate in vbitrates:
        video_configs.append(
            VideoStreamConfig(
                input_file=args['-i'],
                bitrate=vbitrate,
                keyint_min=args['--keyint-min'],
                width=resolutions[str(vbitrate)][0],
                height=resolutions[str(vbitrate)][1],
                output_file=outdir_video+'/'+args['--movie-name']+'_'+str(resolutions[str(vbitrate)][0])+'x'+str(resolutions[str(vbitrate)][1])+'_'+str(vbitrate)+'k.webm'
            )
        )

    for abitrate in abitrates:
        audio_configs.append(
            AudioStreamConfig(
                input_file=args['--audio-file'],
                bitrate=abitrate,
                output_file=outdir_audio+'/'+args['--movie-name']+'_audio_'+str(abitrate)+'k.webm'
            )
        )

    vencoder = EncoderFactory.get_encoder('vp9')
    aencoder = EncoderFactory.get_encoder('vorbis')

    video_tracks = []
    audio_tracks = []

    for job in video_configs:
        vencoder.set_config(job.toDict())
        rcode = vencoder.encode()
        if rcode is 0:
            video_tracks.append(job.output_file)
        else:
            raise RuntimeError('Encoding job for ' + job.input_file + ' failed.')

    for job in audio_configs:
        aencoder.set_config(job.toDict())
        rcode = aencoder.encode()
        if rcode is 0:
            audio_tracks.append(job.output_file)
        else:
            raise RuntimeError('Encoding job for ' + job.input_file + ' failed.')

    segmenter = SegmenterFactory.get_segmenter('webm')

    seg_job = SegmenterConfig(video_tracks=video_tracks, audio_tracks=audio_tracks, output_file=outdir+'/Manifest.mpd')

    segmenter.set_config(seg_job.toDict())
    rcode = segmenter.output_dash()

    if rcode is not 0:
        raise RuntimeError('Segmenter failed.')

